#!/usr/bin/python

execfile('../site.py')
import mapper
import simplejson as json
import sys
import cgi

#form=cgi.FieldStorage()

print "Content-type: application/json\n\n"
try:
    data = json.loads(sys.stdin.read())
except:
    data=dict()
    data['action']='none'
if data['action']=='get_prev':
    print json.dumps({'name':mapper.getLastEntity().name})
elif data['action']=='add_entity':
    #oldID=mapper.getLastEntity().id
    newID=mapper.addEntity(data['type'],data['name'],' ',' ')
    #newrl=mapper.addRelation(oldID,newID,data['relationship'],' ',' ')
    print json.dumps({'id':newID})
elif data['action']=='add_relation':
    relID=mapper.addRelation(data['fromID'],data['toID'],data['relationship'],' ',' ')
    print json.dumps({'fromID':data['fromID'],'toID':data['toID'],'relationshipID':relID})
elif data['action']=='get_entities':
    print json.dumps(mapper.getEntities())
elif data['action']=='get_last_entity':
    print json.dumps({'id':mapper.getLastEntity().id})
elif data['action']=='delete_relationship':
    mapper.delRelation(data['relationshipID'])
    print json.dumps({'deletedID':data['relationshipID']})
elif data['action']=='delete_entity':
    mapper.delEntity(data['entityID'])
    print json.dumps({'deletedID':data['entityID']})
elif data["action"]=='none':
    print mapper.dumpAll()
