#!/usr/bin/python

execfile('../site.py')
from sqlalchemy import *
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

Base = declarative_base()
#Base.metadata.bind = engine

class entity(Base):
    __tablename__ = 'entities'
    id = Column(Integer, primary_key=True)
    type = Column(Enum('"PERSON", "PLACE", "BUSINESS"'))
    name = Column(String(64))
    timestamp = Column(DateTime)
    url = Column(String(256))
    prevurl = Column(String(256))

    def __init__(self, type, name, url, prevurl):
        self.type = type
        self.name=name
        self.url=url
        self.prevurl=prevurl
        self.timestamp='CURRENT_TIMESTAMP'

class relation(Base):
    __tablename__ = 'relationships'
    id = Column(Integer, primary_key=True)
    fromID = Column(Integer)
    to = Column(Integer)
    type = Column(String(64))
    start = Column(DateTime)
    end = Column(DateTime)
    
    def __init__(self, fromID, to, type, start, end):
	self.fromID=fromID
	self.to=to
	self.type=type
	self.start=start
	self.end=end
    
engine = create_engine('mysql://untangld:journalism1@mysql.noahliebman.com/untangld?charset=utf8&use_unicode=0', pool_recycle=3600)
engine.echo=False

entities=entity.__table__
metadata=Base.metadata
metadata.create_all(engine)

Session=sessionmaker(bind=engine)
session=Session()

def addEntity(type,name,url,prevurl):
    newentity=entity(type,name,url,prevurl)
    session.add(newentity)
    session.commit()
    return newentity.id

def addRelation(fromID,to,type,start,end):
    newrelation=relation(fromID,to,type,start,end)
    session.add(newrelation)
    session.commit()
    return newrelation.id
    
def delRelation(relationID):
    session.query(relation).filter_by(id=relationID).delete()
    
def delEntity(entityID):
    session.query(entity).filter_by(id=entityID).delete()
    
def getLastEntity():
    return session.query(entity).order_by(entity.id.desc()).first()
    
def getEntities():
    rows=dict()
    for row in session.query(entity).order_by(entity.id.desc()):
	rows[row.id]={'id':row.id,'name':row.name,'type':row.type}
    return rows
	
def dumpAll():
    string=""
    for row in session.query(entity):
	string+=row.name+" ("+row.type+"):\n"
	for rrow in session.query(relation).filter_by(fromID=row.id):
	    string+="\t"+rrow.type+" - "+session.query(entity).filter_by(id=rrow.to).first().name+" ("+session.query(entity).filter_by(id=rrow.to).first().type+")\n"
    return string
